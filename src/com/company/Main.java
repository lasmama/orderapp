package com.company;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) throws ParseException, IOException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        File file = new File("Orders");
        File file2 = new File("Customers");
//        Customer jauns = new Customer("Janis");
        List<Customer> customerList;
        List<Order> orderList;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file2)))) {
            customerList = reader.lines().map(line -> new Customer(line))
                    .collect(Collectors.toList());
        }
        try (BufferedReader reader1 = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            orderList = reader1.lines().map(line -> Customer.parseToOrder(line, formatter))
                    .collect(Collectors.toList());
        }
        System.out.println(Customer.toMap(customerList, orderList));
//
//            Date date = new Date();
//            Stream<Order> orderStream = orderList.stream().filter((Order o) -> o.isAfter(date));
//            List<Order> collect = orderStream.collect(Collectors.toList());
//            System.out.println(collect);

    }
}
