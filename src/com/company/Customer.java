package com.company;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Customer {
    private final String name;

    private List<Order> orderList = new ArrayList<>();

    public Customer(String Name) {
        this.name = Name;
    }

    public static <Customer, Order> Map<Customer, Order> toMap(List<Customer> customerList, List<Order> orderList) {
        Iterator<Customer> customerIterator = customerList.iterator();
        Iterator<Order> orderIterator = orderList.iterator();
        return IntStream.range(0, customerList.size()).boxed()
                .collect(Collectors.toMap(_i -> customerIterator.next(), _i -> orderIterator.next()));
    }
    //jauna metode addOrder, kas pievienoja jaunus orderus pie Orderlist

    public static Order parseToOrder(String line, SimpleDateFormat formatter) {
        String[] result = line.split("\t");
        return new Order(result[0], getDate(formatter, result[1]));
    }

    private static Date getDate(SimpleDateFormat formatter, String line) {
        try {
            return formatter.parse(line);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public void add(Order item) {
        orderList.add(item);
    }

    @Override
    public String toString() {
        return "Customer{" +
                "name='" + name + '\'' +
                ", orderList=" + orderList +
                '}';
    }

    public String getInfo() {
        return name + orderList.size();
    }

    public String getName() {
        return name;
    }

}

//lauks string name
//Arraylist no orderiem
//japievieno orderus
