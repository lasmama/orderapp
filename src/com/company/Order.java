package com.company;
import java.util.Date;

public class Order {

    private final String description;// lauks description
//    private final Customer customer;

    private final Date submisionDate;

    //    public Order(String orderDescription, Customer customer, Date submisionDate) {
//        this.description = orderDescription;
////        this.customer = customer;
//        this.submisionDate = submisionDate;
//    }
    public Order(String orderDescription, Date submisionDate) {
        this.description = orderDescription;
        this.submisionDate = submisionDate;
    }

    @Override
    public String toString() {
        return "Order{" +
                "description='" + description + '\'' +
                ", submisionDate=" + submisionDate +
                '}';
    }
    public boolean isAfter(Date from) {
        return submisionDate.compareTo(from) >= 0;
    }

}
